(function ($) {
	'use strict';

	  window.app = {
      name: 'trioncube',
      setting: {
        folded: true,
        container: false,
        color: 'primary',
        bg: ''
      }
    };

    baseCuber();

    // init
    function baseCuber(){

      $('body').removeClass($('body').attr('ui-class')).addClass(app.setting.bg).attr('ui-class', app.setting.bg);
      app.setting.folded ? $('#cudeHead').addClass('folded') : $('#cudeHead').removeClass('folded');
      $('#cudeHead').length == 0 && (app.setting.container ? $('.app-header .navbar, .app-content').addClass('container') : $('.app-header .navbar, .app-content').removeClass('container'));

      $('.switcher input[value="'+app.setting.color+'"]').prop('checked', true);
      $('.switcher input[value="'+app.setting.bg+'"]').prop('checked', true);

      $('[data-target="folded"] input').prop('checked', app.setting.folded);
      $('[data-target="container"] input').prop('checked', app.setting.container);
      
      // if(color != app.setting.color){
      //   uiLoad.remove('css/theme/'+color+'.css');
      //   uiLoad.load('css/theme/'+app.setting.color+'.css');
      //   color = app.setting.color;
      // }
    }


    // var request = $.ajax({
    //   url: "https://outbeast.com/blog/email.json",
    //   method: "GET",
    //   dataType: "json"
    // });
     
    // request.done(function( msg ) {
    //   $( "#log" ).html( msg );
    // });
     
    // request.fail(function( jqXHR, textStatus ) {
    //   alert( "Request failed: " + textStatus );
    // });


    $("#grabEmailData").click(function(e) {
        e.preventDefault();
        // $.ajax({
        //     type: "GET",
        //     url: "https://outbeast.com/blog/email.json",
        //     data: { 
        //         id: $(this).val(), // < note use of 'this' here
        //         access_token: $("#access_token").val() 
        //     },
        //     success: function(result) {
        //         alert('ok');
        //     },
        //     error: function(result) {
        //         alert('error');
        //     }
        // });
         $.ajax({
            type: "POST",
            url: 'email.json',
            dataType: 'json',
            data: { 
                accountid: ("686867866876876"), // < note use of 'this' here
                access_token: ("gijhgi123123123123ghjghjgjgjg12313")
            },
            success: function(response){
                console.table([response]);
            },
            error: function(result) {
                alert('error');
            }
         });        
    });

    $( "#dashboardOubBeast" ).click(function() {
      $( "#dashboardbox" ).removeClass( "displayNone" );
      $( "#searchtoolbox" ).addClass( "displayNone" );
      $( "#domainfinder" ).addClass( "displayNone" );
      $( "#locationfinder" ).addClass( "displayNone" );
      $( "#ProspectInsight_Shell" ).addClass( "displayNone" );
      $( "#LookUpPlus_Shell" ).addClass( "displayNone" );
      $( "#campaignsystem" ).addClass( "displayNone" );
      $( "#contactbox" ).addClass( "displayNone" );
      $( "#createcampaign" ).addClass( "displayNone" );


    });

    $( "#prospectsOubBeast" ).click(function() {

      $( "#dashboardbox" ).addClass( "displayNone" );
      $( "#searchtoolbox" ).removeClass( "displayNone" );
      $( "#domainfinder" ).addClass( "displayNone" );
      $( "#locationfinder" ).addClass( "displayNone" );
      $( "#ProspectInsight_Shell" ).addClass( "displayNone" );
      $( "#LookUpPlus_Shell" ).addClass( "displayNone" );
      $( "#campaignsystem" ).addClass( "displayNone" );
      $( "#contactbox" ).addClass( "displayNone" );
      $( "#createcampaign" ).addClass( "displayNone" );


    });


    $( "#campaignOubBeast" ).click(function() {

      $( "#dashboardbox" ).addClass( "displayNone" );
      $( "#searchtoolbox" ).addClass( "displayNone" );
      $( "#domainfinder" ).addClass( "displayNone" );
      $( "#locationfinder" ).addClass( "displayNone" );
      $( "#ProspectInsight_Shell" ).addClass( "displayNone" );
      $( "#LookUpPlus_Shell" ).addClass( "displayNone" );
      $( "#campaignsystem" ).removeClass( "displayNone" );
      $( "#contactbox" ).addClass( "displayNone" );
      $( "#createcampaign" ).addClass( "displayNone" );


    });

        $( "#contactOubBeast" ).click(function() {

      $( "#dashboardbox" ).addClass( "displayNone" );
      $( "#searchtoolbox" ).addClass( "displayNone" );
      $( "#domainfinder" ).addClass( "displayNone" );
      $( "#locationfinder" ).addClass( "displayNone" );
      $( "#ProspectInsight_Shell" ).addClass( "displayNone" );
      $( "#LookUpPlus_Shell" ).addClass( "displayNone" );
      $( "#campaignsystem" ).addClass( "displayNone" );
      $( "#contactbox" ).removeClass( "displayNone" );
      $( "#createcampaign" ).addClass( "displayNone" );


    });

            $( ".finderdomain" ).click(function() {
      $( "#dashboardbox" ).addClass( "displayNone" );
      $( "#searchtoolbox" ).addClass( "displayNone" );
      $( "#domainfinder" ).removeClass( "displayNone" );
      $( "#locationfinder" ).addClass( "displayNone" );
      $( "#ProspectInsight_Shell" ).addClass( "displayNone" );
      $( "#LookUpPlus_Shell" ).addClass( "displayNone" );
      $( "#campaignsystem" ).addClass( "displayNone" );
      $( "#contactbox" ).addClass( "displayNone" );
      $( "#createcampaign" ).addClass( "displayNone" );


    });





                $( ".orginizationlookup" ).click(function() {

      $( "#dashboardbox" ).addClass( "displayNone" );
      $( "#searchtoolbox" ).addClass( "displayNone" );
      $( "#domainfinder" ).addClass( "displayNone" );
      $( "#locationfinder" ).addClass( "displayNone" );
      $( "#ProspectInsight_Shell" ).addClass( "displayNone" );
      $( "#LookUpPlus_Shell" ).removeClass( "displayNone" );
      $( "#campaignsystem" ).addClass( "displayNone" );
      $( "#contactbox" ).addClass( "displayNone" );
      $( "#createcampaign" ).addClass( "displayNone" );

     });
    $( ".locationsearch" ).click(function() {

      $( "#dashboardbox" ).addClass( "displayNone" );
      $( "#searchtoolbox" ).addClass( "displayNone" );
      $( "#domainfinder" ).addClass( "displayNone" );
      $( "#locationfinder" ).removeClass( "displayNone" );
      $( "#ProspectInsight_Shell" ).addClass( "displayNone" );
      $( "#LookUpPlus_Shell" ).addClass( "displayNone" );
      $( "#campaignsystem" ).addClass( "displayNone" );
      $( "#contactbox" ).addClass( "displayNone" );
      $( "#createcampaign" ).addClass( "displayNone" );


    });

    $( ".prospectInsight" ).click(function() {

      $( "#dashboardbox" ).addClass( "displayNone" );
      $( "#searchtoolbox" ).addClass( "displayNone" );
      $( "#domainfinder" ).addClass( "displayNone" );
      $( "#locationfinder" ).addClass( "displayNone" );
      $( "#ProspectInsight_Shell" ).removeClass( "displayNone" );
      $( "#LookUpPlus_Shell" ).addClass( "displayNone" );
      $( "#campaignsystem" ).addClass( "displayNone" );
      $( "#contactbox" ).addClass( "displayNone" );
      $( "#createcampaign" ).addClass( "displayNone" );


    });


})(jQuery);
