(function ($) {
  'use strict';

    window.app = {
      name: 'trioncube',
      setting: {
        folded: true,
        container: false,
        color: 'primary',
        bg: ''
      }
    };

    baseCuber();

    // init
    function baseCuber(){

      $('body').removeClass($('body').attr('ui-class')).addClass(app.setting.bg).attr('ui-class', app.setting.bg);
      app.setting.folded ? $('#cudeHead').addClass('folded') : $('#cudeHead').removeClass('folded');
      $('#cudeHead').length == 0 && (app.setting.container ? $('.app-header .navbar, .app-content').addClass('container') : $('.app-header .navbar, .app-content').removeClass('container'));

      $('.switcher input[value="'+app.setting.color+'"]').prop('checked', true);
      $('.switcher input[value="'+app.setting.bg+'"]').prop('checked', true);

      $('[data-target="folded"] input').prop('checked', app.setting.folded);
      $('[data-target="container"] input').prop('checked', app.setting.container);
    
    }


    // var request = $.ajax({
    //   url: "https://outbeast.com/blog/email.json",
    //   method: "GET",
    //   dataType: "json"
    // });
     
    // request.done(function( msg ) {
    //   $( "#log" ).html( msg );
    // });
     
    // request.fail(function( jqXHR, textStatus ) {
    //   alert( "Request failed: " + textStatus );
    // });


    $("#grabEmailData").click(function(e) {
        e.preventDefault();
        // $.ajax({
        //     type: "GET",
        //     url: "https://outbeast.com/blog/email.json",
        //     data: { 
        //         id: $(this).val(), // < note use of 'this' here
        //         access_token: $("#access_token").val() 
        //     },
        //     success: function(result) {
        //         alert('ok');
        //     },
        //     error: function(result) {
        //         alert('error');
        //     }
        // });
         $.ajax({
            type: "POST",
            url: 'email.json',
            dataType: 'json',
            data: { 
                accountid: ("686867866876876"), // < note use of 'this' here
                access_token: ("gijhgi123123123123ghjghjgjgjg12313")
            },
            success: function(response){
                console.table([response]);
            },
            error: function(result) {
                alert('error');
            }
         });        
    });

    // dataBeast will Get the data, render it.
    var dataBeastGet = function(source, template, resultPrint){
      alert(source);
        $.getJSON(source, function(data) {
          var template = $(template).html();
          var dataRendered = Handlebars.compile(template)(data);
          $(resultPrint).append(dataRendered);
       });
    }

    // dataBeast will Post the data, render it.
    var dataBeastPost = function(source, template, resultPrint){
        $.getJSON(source, function(data) {
          alert(source);
          var template = $(template).html();
          var dataRendered = Handlebars.compile(template)(data);
          $(resultPrint).append(dataRendered);
       });
    }



    var dataBeastRendered = function(data, template, resultPrint){
          alert(data);
          alert(template);
          alert(resultPrint);
          var template =  $(template).html();
          var stone = Handlebars.compile(template)({name: "Luke", power: "force"});
          $(resultPrint).append(stone);          
          // var dataTest = {"name": "meadowlark.js"};
          // var template = $(template).html();
          // var dataRendered = Handlebars.compile(template)(dataTest);
          // $(resultPrint).append(dataRendered);
          // alert('data');
    }


    $( "#dashboardOubBeast" ).click(function() {
        $(".appContentBox").empty();
        var postPayload = {dashboard :"dashboard"}
        $.post("email.json", postPayload, function(data) {
            alert(JSON.stringify(postPayload));
            var context = JSON.stringify(data);
            var template =  $('#outBeastDashboardContainer').html();
            var stone = Handlebars.compile(template)(context);
            $(".appContentBox").append(stone);
        });

      // dataBeast('data.json', '#tpl', '#anchor'); // since url = 'data.json' , we can use both notations.
    });

    $( "#searchToolsOubBeast" ).click(function() {
        $(".appContentBox").empty();
        var template =  $('#outBeast-SearchTool-Container').html();
        var stone = Handlebars.compile(template);
        $(".appContentBox").append(stone);
      // dataBeast('data.json', '#tpl', '#anchor'); // since url = 'data.json' , we can use both notations.
    });

    $('body').on('click', "#prospectInsighSearch", function() {
        $(".appContentBox").empty();
        var template =  $('#outBeast-ProspectInsight-Container').html();
        var stone = Handlebars.compile(template);
        $(".appContentBox").append(stone);
      // dataBeast('data.json', '#tpl', '#anchor'); // since url = 'data.json' , we can use both notations.
    });

    $('body').on('click', "#locationsearchId", function() {
        $(".appContentBox").empty();
        var template =  $('#outBeast-LocationSearch-Container').html();
        var stone = Handlebars.compile(template);
        $(".appContentBox").append(stone);
      // dataBeast('data.json', '#tpl', '#anchor'); // since url = 'data.json' , we can use both notations.
    });    

    $('body').on('click', "#orginizationlookupId", function() {
        $(".appContentBox").empty();
        var template =  $('#outBeast-LookUp-Container').html();
        var stone = Handlebars.compile(template);
        $(".appContentBox").append(stone);
      // dataBeast('data.json', '#tpl', '#anchor'); // since url = 'data.json' , we can use both notations.
    });    

    $('body').on('click', "#finderdomainSearch", function() {
        $(".appContentBox").empty();
        var template =  $('#outBeast-DomainSearch-Container').html();
        var stone = Handlebars.compile(template);
        $(".appContentBox").append(stone);
      // dataBeast('data.json', '#tpl', '#anchor'); // since url = 'data.json' , we can use both notations.
    });    

    $('body').on('click', "#contactOubBeast", function() {

        $(".appContentBox").empty();
        var template =  $('#outBeast-Contact-Container').html();
        var stone = Handlebars.compile(template);
        $(".appContentBox").append(stone);
      // dataBeast('data.json', '#tpl', '#anchor'); // since url = 'data.json' , we can use both notations.
    }); 

    $('body').on('click', "#grabEmailData", function(){    
        var postPayload = {prospectInsightInput : $("#prospectInsightInput").val()}
        $.post("email.json", postPayload, function(data) {
          alert(JSON.stringify(postPayload));
            var context = JSON.stringify(data);
            dataBeastRendered(context, '#outBeast-ProspectData-Container', '#ProspectInsight_Shell .dataFetchBox');
        });
    });

    $('body').on('click', "#grabLookUpData", function(){    
      var postPayload = {lookupInput : $("#lookupInput").val()}
        $.post("email.json", postPayload, function(data) {
          alert(JSON.stringify(postPayload));
            var context = JSON.stringify(data);
            dataBeastRendered(context, '#outBeast-LookUpData-Container', '#LookUpPlus_Shell .dataFetchBox');
        });
    });

    $('body').on('click', "#grabDomainData", function(){    
      var postPayload = {domainFinderInput : $("#domainFinderInput").val()}
        $.post("email.json", postPayload, function(data) {
          alert(JSON.stringify(postPayload));
            var context = JSON.stringify(data);
            dataBeastRendered(context, '#outBeast-DomainData-Container', '#domainfinder .dataFetchBox');
        });
    });

    $('body').on('click', "#grabLocationData", function(){ 
        var postPayload = { locationInputKeyword : $("#locationInputKeyword").val(), locationInputBusinessLocation : $("#locationInputBusinessLocation").val() }
        $.post("email.json", postPayload, function(data) {
          alert(JSON.stringify(postPayload));
            var context = JSON.stringify(data);
            dataBeastRendered(context, '#outBeast-LocationData-Container', '#locationfinder .dataFetchBox');
        });
    });    


$( "#grabEmailData1" ).submit(function( event ) {
 
  // Stop form from submitting normally
  event.preventDefault();
 
  // Get some values from elements on the page:
  var $form = $( this ),
    term = $form.find( "input[name='s']" ).val(),
    url = $form.attr( "action" );
 
  // Send the data using post
  var posting = $.post( url, { s: term } );
 
  // Put the results in a div
  posting.done(function( data ) {
    dataBeastPost('email.json', 'outBeast-ProspectData-Container', 'dataFetchBox');
  });
});


})(jQuery);